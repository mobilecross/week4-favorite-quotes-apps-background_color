import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';

/**
 * Generated class for the QuotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quote',
  templateUrl: 'quote.html',
})
export class QuotePage {
  quoteText : string;
  quotePerson: string;
  constructor(private viewCtr:ViewController,private navParams:NavParams) {
  }

  ionViewDidLoad() {
    this.quoteText = this.navParams.get('quoteText');
    this.quotePerson =this.navParams.get('quotePerson');
    console.log(this.quoteText);
    console.log('ionViewDidLoad QuotePage');
  }

  closeModal(remove:boolean){
    this.viewCtr.dismiss(remove);
    console.log(remove);
  }

}
